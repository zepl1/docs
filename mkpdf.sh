#! /usr/bin/env bash

cat STEP-0.md STEP-1.md STEP-2.md STEP-3.md STEP-4.md > all.md
pandoc --variable urlcolor=cyan --toc --toc-depth=2 -N title.md all.md -o all.pdf
rm all.md
