#!/usr/bin/env python
import sys
from threading import Thread

import asyncio, websockets
import zmq
import zmq.asyncio

uri_sync_to_async = 'ipc://@w2r'

class WsToRelay():
    loop = asyncio.get_event_loop()
    loop.set_debug(True)

    ctx = zmq.asyncio.Context()
    server = ctx.socket(zmq.PAIR)

    def __init__(self, uri):
        self.server.bind(uri)

    async def ws_sender(self, websocket):
        while True:
            msg = await self.server.recv()
            await websocket.send(msg)

    async def ws_receiver(self, websocket):
        async for msg in websocket:
            await self.server.send(msg)

    async def ws_handler(self, uri):
        async with websockets.connect(uri) as websocket:
            await asyncio.gather(
                    self.ws_sender(websocket),
                    self.ws_receiver(websocket))

class UserIO():
    uri_reader = 'inproc://reader'
    uri_writer = 'inproc://writer'

    ctx = zmq.Context()

    user_in = ctx.socket(zmq.PAIR)
    user_in.connect(uri_reader)

    user_out = ctx.socket(zmq.PAIR)
    user_out.connect(uri_writer)

    reader2ws = ctx.socket(zmq.PAIR)
    reader2ws.bind(uri_reader)
    ws2writer = ctx.socket(zmq.PAIR)
    ws2writer.bind(uri_writer)

    ws = ctx.socket(zmq.PAIR)

    poller = zmq.Poller()
    poller.register(reader2ws, zmq.POLLIN)
    poller.register(ws, zmq.POLLIN)

    def __init__(self, ws_server):
        self.ws.connect(ws_server)

    def relay(self):
        while True:
            socks = dict(self.poller.poll(100))

            if self.reader2ws in socks.keys():
                # send to ws_handler
                msg = self.reader2ws.recv()
                self.ws.send(msg)

            if self.ws in socks.keys():
                # recv from ws_handler
                msg = self.ws.recv()
                self.ws2writer.send(msg)

    def reader(self):
        while True:
            for raw in sys.stdin:
                self.user_in.send(raw.encode())
    
    def writer(self):
        while True:
            raw = self.user_out.recv()
            print(f'{raw.decode()}', end='', flush=True)

if __name__ == '__main__':

    uio = UserIO(uri_sync_to_async)
    reader = Thread(target=uio.reader)
    reader.start()
    writer = Thread(target=uio.writer)
    writer.start()
    u2ws = Thread(target=uio.relay)
    u2ws.start()

    loop = asyncio.get_event_loop()
    r = WsToRelay(uri_sync_to_async)
    loop.create_task(r.ws_handler('ws://localhost:8266'))

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        reader.join()
        writer.join()
        u2ws.join()
