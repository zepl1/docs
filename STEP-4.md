\newpage

# STEP-4 The And

After the refactor was done i merged the protocol and device code into a single repository [(zepl-device)](https://gitlab.com/zepl1/zepl-device) and made a bit more refined version of
[zepl-broker](https://gitlab.com/zepl1/zepl-broker) with some tooling for simulating different workloads and a crappy client. Look at the docs there for more information.

If you find a problem with the code please open an issue with the description of the problem you've encountered as title. If applicable, include some code to reproduce your problem.

The whole Project took about two weeks of time from writing the first line of code to writing the last sentence of this document. This was way longer than i wanted it to take but after starting to write
i wanted the story to have a happy ending.
