\newpage

# STEP-0 Prelude

I wanted to write a small beginners tutorial showcasing 0MQ's ability to solve small and simple tasks. I enjoyed writing this and somehow ended up with a steep dive into it. Take it for what it is.
STEP-1 and STEP-2 are insight on the process of how [Zepl](https://gitlab.com/zepl1/zepl) and [zepl-broker-prototype](https://gitlab.com/zepl1/zepl-broker-prototype) grew from prototype to a usable application.
STEP-3 and STEP-4 were done for the sake of completeness.

## MicroPython WebREPL
For those alien to the world of MicroPython, its basically a tiny implementation of Python for embedded devices. WebREPL is basically like a serial interface via websockets which can give you access
to the python shell and uploading/downloading files to/from your target board. Given the right tooling it is a very speedy process (no compiling, only flashing single scripts).

## Setup
It is recommended to set up [WebREPL](https://docs.micropython.org/en/latest/esp8266/tutorial/repl.html#webrepl-a-prompt-over-wifi) on your MicroPython board(s) to follow along.

If you don't have any MicroPython hardware, you can play around with the [dummy](https://gitlab.com/zepl1/zepl-broker) from the 'finished' application. Use and abuse it to your liking.
