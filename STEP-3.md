\newpage

# STEP-3 The End

The first part of the refactor is well summarized by this picture:

![Refactor Summary.](pics/refac.png)

The ZeplDevice class handles loading the classes. IoRunner and DeviceRunner have a symmetrical structure, 0MQ plumbing is done here.
WebReplDevice and WebReplIo contain the implementation details of the device and protocol, which in our case is the communication with our MicroPython app
via the WebREPL protocol.

Refactoring takes time, pen, paper and coffee but usually is very rewarding work. Slowly, we are starting to see structure in chaos and also...

\newpage

### Problem: Sending data to the device runner does not work at all.
It almost looks like it was never tested at all!

#### Solution:
More [coffee!!](https://gitlab.com/zepl1/zepl-broker-prototype/-/commit/125145aafc881d898c6f470c0c1add7b5031adcf)
Somehow this one slipped through. I suck at multitasking so i blame the writing of this story for it :)

With this short patch we should theoretically be able to support hundreds of devices and controllers at once. Let's take a closer look at how, with the power of ZeroMQ and little bit of finesse,
we can ensure that each message ends up where it should.
When receiving from the ROUTER socket we get a uniqe id added as envelope by zeromq to identify the connected client. We send this id as part of the message we distribute to the devices.
When receiving from the devices we get back an additional that marks their origin. This way our broker, device input and output handlers boil down to ~30 lines of code each. _Slick_.

After some more minor adjustments where do we end up? There are still some rough edges, but i think i made my point regarding ZMQ. From this point on we can see a lot of paths opening up.
A toolkit for developing IoT protocols? A service for distributing applications to embedded devices? A distributed data logger? Or you could add a topic all device runners listen to, to synchronize multiple
devices with multiple protocols at once. I find the configuration file is quite annoying to deal with ([get rid of it!](https://rfc.zeromq.org/spec/12/)). The elephant in the room is
the lack of a formal specification of the MicroPython protocol which you can clearly see in the code just by looking at it. But for now, i am just glad i can use my RaspberryPi to toggle my
desktop pc's power switch with an esp8285 wired inside of it.
