\newpage

# STEP-2

This is the continuation of [STEP-1](STEP-1.md). You can browse the code in it's state relevant for this document
[here](https://gitlab.com/zepl1/zepl-broker-prototype/-/tree/END-STEP-2) or, if you have cloned the repository with `git checkout tags/END-STEP-2`. After that commit a refactor was done.

## Idea
Let's build a little 0MQ demo application they said, it will be fun they said.

\ 

> "Ideas are cheap"
> -- <cite>[Pieter Hintjens](http://zguide.zeromq.org/page:all#Infinite-Property)</cite>

\ 

![The Idea.](pics/idea.png)


We want to reuse the websocket handler and protocol from zepl and make use of WebREPL's command set. We do not want to fiddle around with threads anymore. Instead we want to fiddle around with asyncio only.
This way we have much less fiddling to do! the point i am trying to make here is, there are a bazillion ways to do this using thousand of other tools available but using 0MQ helps us breaking
down the problem in a _natural way_, which in turn enables us to develop accurate solutions by climbing the hill one step at a time.

\newpage

## Sketch

![First Sketch.](pics/sketch.png)

Sketching out your architecture should be an initial guess of how to break down the problem by thinking in terms of messages being passed between message processing tasks. We added two prototypical applications
one we call `log_listener` and another one called `controller`. logging is useful for many reasons and one can think of endless scenarios for controlling a device (or the other way round) so we'll include that.
without assuming any implementation details.

These considerations about the actual application starts giving structure to how we picture our application, this is also often a trap. starting with too many assumptions will lead to shit, closed source software,
so called "open standards" owned by corporations and other menaces to society so we try to leave assumptions aside and decide to
build a broker architecture to connect our dummy applications together. If you read [The Guide](http://zguide.zeromq.org/page:all) you will know that if you can afford it
you should steer away from broker architectures whenever possible. However in this case we should be good, as long as we _keep it simple_...

\newpage

### Plumbing
After you have a rough idea of how your application should look, it's time to look at how to connect the pieces.
I strongly recommend reading [messaging patterns](http://zguide.zeromq.org/page:all#Messaging-Patterns), or at least playing around with some toy examples as found in
[Learning ØMQ with pyzmq](https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/pyzmq.html).

connecting most of the pieces is straightforward teh zmq_runner connects to the protocol handler via pair, that is a given. the demo applications, since for now they do nothing
tie in pretty easily by using a PUB/SUB and ROUTER/DEALER pattern.

since we'd like to support more than one device we chose to distribute the messages to and from the zmq_runners via PUB/SUB patterns (one for every direction).

We update our sketch to include this topology. The arrows point to the part that binds, all unlabeled transports are _inproc_.

![Plumbing.](pics/plumbing.png)

Now You're Thinking With Portals.

\newpage

## Doin' the Do
> Because in the end, "code talks, bullshit walks". People can complain and
> suggest alternatives all they want, but you can't just argue. At some
> point you need to show the code that actually solves the problem.
>
> -- <cite>[LKML](https://lkml.org/lkml/2010/6/10/77)</cite>

now we actually start writing code. we build a class called ZmqRunner which starts up an instance of WsToRelay (from zepl), sets up the plumbing and runs a seperate poll loop to provide
a place acting on incoming and outgoing messages. we also build the ZmqBroker class, our central piece which basically acts as something you could call a message switch, all incoming
messages from the SUB socket will be published on "tcp://0.0.0.0:9000", and all incoming ROUTER messages from "tcp://0.0.0.0:9001" call a method called `serve()` which we do not care about for now.
again we end up with a skeleton application that lacks some meat to be able to walk.

### Problem: Now I have some coroutines waiting around and nothing happens.
#### Solution
looking back at our example applications again we see that we want to see logs and _do something_. We want to keep as much stateful operations away from the broker, so we can put most of the logic into the device runner.
i wrote a small [MicroPython app](../mpy) that implements a simple message based state machine that helps us doing just that. the main loop waits for a single input character (both WebREPL and Serial work)
and executes a specific state based on it. Here is an overview of these states:

| input | state       | example output				|
|:------|:-----------:|:----------------------------------------|
| 1 	| DEV_ID      | `DEV_ID 30aea47424bc`			|
| 2 	| F_HTAB      | `F_HTAB {"boot.py": "<hash>", ... }`	|
| 3 	| IDLE        | `LOG sleeping 5 seconds`		|
| a 	| run app a   | `LOG app_a finished`			|
| b 	| run app b   | `ERROR Could not Mount SD Card!`	|

* `DEV_ID`: unique id of the board used as topic for pub/sub
* `F_HTAB`: json serialized string that contains all files in `/` on the board and hex strings of their sha-1 hash
* `IDLE`: idle state, sends `HEART BEAT` and `LOG sleeping 5 seconds` every 5 seconds...
* app a and b can be modified to whatever you want them to be...

not we add methods to our device runner to get the board uuid, creating a hashtable of the local files and using these to decide of wether to upload them or not. the broker creates a hastable
of the device instances and when the controller application is asking for it (by sending the device id and the sync command) it executes the sync_files function for that device.

### Problem: Now what?
#### Solution
That's it. We're done (_again_). We have a small demo application built on top of zepl that can distribute and execute MicroPython applications. We skipped on implementing something like a hearbeat, because that is something
that should be done on the protocol level (WebReplIo). The broker class has around 100 lines of code and the runer has around 200.
Neat.

However we have added a lot of messy code on top of zepl. So we should have a veeery close look at the code and see how we can simplify things... [STEP-3](STEP-3.md)
