\newpage

# STEP-1

You can browse the code in it's state relevant of this document [here](https://gitlab.com/zepl1/zepl/-/tree/END-STEP-1), or, if you have cloned
the repository with `git checkout tags/END-STEP-1`. After that commit a refactor was done.

## About ZeroMQ

\ 

> ZeroMQ (also spelled ØMQ, 0MQ or ZMQ) is a high-performance asynchronous messaging library, aimed at use in distributed or concurrent applications.
> It provides a message queue, but unlike message-oriented middleware, a ZeroMQ system can run without a dedicated message broker.
>
> -- <cite>Wikipedia</cite>

\ 

Read [The Guide](http://zguide.zeromq.org/page:all) (read it)!

## From Problem to Prototype

\ 

> "Ideas are cheap"
> -- <cite>[Pieter Hintjens](http://zguide.zeromq.org/page:all#Infinite-Property)</cite>

\ 

If you haven't read the guide yet, at least read the section on
[MOPED](http://zguide.zeromq.org/page:all#Message-Oriented-Pattern-for-Elastic-Design)
and
[SOD](http://zguide.zeromq.org/page:all#Simplicity-Oriented-Design).

### Problem: I cannot use WebREPL as CLI terminal application.

there are some CLI applications ([[official](https://github.com/kost/webrepl-python)]/[[kost](https://github.com/micropython/webrepl)])
for WebREPL but these can only send/receive files/strings synchronously (a js terminal application exists, so in theory this should be possible using python).

#### Solution:

Build one! Start with a prototype. Before building a prototype, define an architecture.

We want to split the application logic into two parts -- User and Websockets. Splitting the application logic this way enables us to separate the nasty protocol stuff from the nasty user interface stuff.

We want the "user" and the "websockets" part communication to be asychronous (that is: being able to send and receive whenever we want).

There is a well maintained, enterprise-backed, asynchronous implementation of [websockets](https://github.com/aaugustin/websockets) in python, we should use it.

We will build our prototype around a websockets echo server, deal with the user facing stuff first which should be the easier problem to tackle.
This enables us to "emulate" one half of the architecture using boilerplate/example code see: [echo server](https://gitlab.com/zepl1/zepl/-/blob/END-STEP-1/doc/prototype/echo.py) and
[class WsToRelay():](https://gitlab.com/zepl1/zepl/-/blob/END-STEP-1/doc/prototype/prototype.py))
(i will not go into any details of asyncio). This way we can spawn the whole event loop in a seperate thread and not worry about it, while focusing on much more pressing problems...

### Problem: stdin and stdout/stderr are synchronous.

Or: How to print and read simultaneously with python? A problem that never occured to me up until this project.

#### Solution:

Use threads, one as input one as output.

### Problem: Now I have some threads waiting around and nothing happens.

#### Solution:

Use ZeroMQ to connect the pieces. Make them talk to each other.

For getting started with 0MQ and getting to know Python's threading and multiprocessing a good starting point is: [Learning ØMQ with pyzmq](https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/pyzmq.html).
Some small parts of it's information is outdated but it's still a very well written (and short) read for getting a grasp of the basics.

Here is a quick rundown:

From a programming perspective ZeroMQ is basically sockets on Steroids. As an example open two Python shells and send messages.
```
>>> import zmq
>>> c = zmq.Context()
>>> s = c.socket(zmq.PAIR)
>>> s.bind('tcp://0.0.0.0:9000')
>>> s.send(b'hi')
>>>
```
```
>>> import zmq
>>> c = zmq.Context()
>>> s = c.socket(zmq.PAIR)
>>> s.connect('tcp://localhost:9000')
>>> s.recv()
b'hi'
>>>
```
`c = zmq.Context()` is always needed, it handles the messages in the background (usually one Context per Process). Contexts __are__ _threadsafe_.

`s = c.socket(<socket type>)` creates a communication interface, the argument defines the type. Sockets are __not__ _threadsafe_

`zmq.PAIR` is basically just a one-to-one connection. Others (PUSH/PULL, ROUTER/DEALER, ...) are available but here we only need 1-to-1 connections (Read [The Guide](http://zguide.zeromq.org/page:all))

`s.bind(<uri>)/s.connect(<uri>)` only one can bind, depending on socket type many can connect (think: master/slave)

`<uri>` the main transport you should know are:

* 'inproc:// communication between threads (same context)
* 'ipc:// inter process communication ('system wide') -- '@' prefix for abstract name (Linux only) or path to a file (e.g. /var/run/myipc)
* 'tcp:// network address and port
* changing between ipc and tcp is always possible in 0MQ, this enables us to write multiprocessing distributed applications!

`s.send(<message>)/s.recv()` send and receive messages

That's it! Now we can connect the pieces:

![ZeroMQ Plumbing.](pics/zmq.png)

This picture shows the 0MQ transports used and who establishes the connection (the arrow points to the part that binds). All sockets are zmq.PAIR.

To connect User I/O to the websockets we introduced an additional thread (relay) that serves as 'dumb' forwarder for messages.
Look at the [relay method](https://gitlab.com/zepl1/zepl/-/blob/END-STEP-1/doc/prototype/prototype.py#L62). On the websockets side we can use zmq.asyncio.Context and just await the send and receive methods.

\newpage

### Prototype
Our "working" Prototype now looks like this:

![Prototype.](pics/prototype.png)

you can [try it](https://gitlab.com/zepl1/zepl/-/blob/END-STEP-1/doc/prototype/prototype.py) if you are brave. You can also try to connect to a WebREPL server and find that it already kinda works, but not really...

## From Prototype to 'finished' Application
Enough fooling around. Time to get to the meat of it.

### Problem: Character coding is hard.

Handling control characters (esp. Ctrl+C) is driving me crazy!

#### Solution:

You are not alone. [Thankfully this stuff is well figured out](https://github.com/pyserial/pyserial/blob/master/serial/tools/miniterm.py).
If you are interested, take a look, it's worth it.

Now, we have a help menu, and all characters are handeled correctly. When we connect to a WebREPL instead of the echo server we are greeted with a password promt and can send and receive characters between the two. _Nice_.

### Problem: Cannot send/receive files.

#### Solution:

Looking at [this](https://github.com/kost/webrepl-python), [this](https://github.com/micropython/webrepl) and [especially this](https://github.com/micropython/micropython/blob/master/extmod/modwebrepl.c),
I managed to "emulate" the expected behaviour of the Protocol.

The functionality (get, put, cmd) will be imlpemented as a kind of codec to propagate the state from the user to the websocket because the protocol forces snychronicity upon us. To do this we make use of 0MQ's multipart messaging.
Instead of sending only characters we now send lists, we then iterate over the list to figure out what this message means for the internal state. Look at [the code](../../src/zepl/webrepl_proto.py#L107) to see it in action.
Now, we support all of the functionality of WebREPL and proceed building applications on top of it.

### Problem: Websocket disconnect breaks application.

#### Solution:

Have a look at [this exemplary issue](https://gitlab.com/zepl1/zepl/-/issues/1) and
[non exemplary commit](https://gitlab.com/zepl1/zepl/-/commit/0df6c621f1922e922c2950524d01e0c499cef4f3#53951bae24267f0e901e626a1c53ab13db32a981) (only: remote_repl.py) for the solution

## Going Deeper
At this point zepl can be considered finished. We hit a local maximum in the hill-climbing algorithm. We ended up with a nicely working application (uploading files to the board seem to work better than with ampy)
by solving _real_ problems. one thing left is, that the code got messy. It always gets messy when you do _real_ work. But this application is so small, it wouldn't be too bothersome to make changes
in the future, so there is no drive to tackle this problem. You need __chaos__ to produce __order__.

So i made a small [demo application](https://gitlab.com/zepl1/zepl-broker-prototype). The goal was to have a small, networked application that can handle multiple devices at once.
This created enough ripples to make a refactor come natural. If you are interested in the story of Zepl Broker you can continue reading [STEP-2](STEP-2.md).
You will see ZMQ reveal it's power by reusing and connecting code in different patterns. If you haven't already, build some toy examples yourself. Go do it!
